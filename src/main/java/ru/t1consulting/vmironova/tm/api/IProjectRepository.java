package ru.t1consulting.vmironova.tm.api;

import ru.t1consulting.vmironova.tm.model.Project;

import java.util.List;

public interface IProjectRepository {

    Project add(Project project);

    void clear();

    List<Project> findAll();

}
