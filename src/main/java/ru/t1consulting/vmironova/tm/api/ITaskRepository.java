package ru.t1consulting.vmironova.tm.api;

import ru.t1consulting.vmironova.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    Task add(Task task);

    void clear();

    List<Task> findAll();
}
