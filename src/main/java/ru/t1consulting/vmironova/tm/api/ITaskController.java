package ru.t1consulting.vmironova.tm.api;

public interface ITaskController {

    void createTask();

    void clearTasks();

    void showTasks();

}
