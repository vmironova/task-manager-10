package ru.t1consulting.vmironova.tm.api;

public interface IProjectController {

    void createProject();

    void clearProjects();

    void showProjects();

}
