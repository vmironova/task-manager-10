package ru.t1consulting.vmironova.tm.api;

import ru.t1consulting.vmironova.tm.model.Task;

import java.util.List;

public interface ITaskService {

    Task add(Task task);

    Task create(String name);

    Task create(String name, String description);

    void clear();

    List<Task> findAll();

}
