package ru.t1consulting.vmironova.tm.api;

import ru.t1consulting.vmironova.tm.model.Project;

import java.util.List;

public interface IProjectService {

    Project add(Project project);

    Project create(String name);

    Project create(String name, String description);

    void clear();

    List<Project> findAll();

}
